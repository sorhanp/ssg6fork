# ssg6fork

A fork of static site generator by Roman Zolotarev known as [ssg6](https://rgz.ee/ssg.html)

## About

ssg6fork is a static site generator, with support for markdown files (.md), that can be run in shell. The original ssg6 required lowdown(1) or Markdown.pl to parse .md files to HTML.

## How to use

TODO